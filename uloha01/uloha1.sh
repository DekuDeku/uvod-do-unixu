#Filip Rechtorík

#Using touch
touch --date="last week" /tmp/lwf
find -type f -newermm /tmp/lwf

#AFAIK this does the same thing as above
#thanks to minus before 7
find -type f -mtime -7

#For finding by birth time, although I think in schools
#filesystem this is not supported
touch --date="last week" /tmp/lwf
find -type f -newerBm /tmp/lwf
