#!/bin/sh

#1.
find /etc -maxdepth 1 -type f -printf '%s\t%p\n' 2>/dev/null | sort -k1 -n -r | head -n1 | cut -f 2

#2.
find /etc -type f -printf '%s\t%p\n' 2>/dev/null | sort -k1 -n -r | head -n1 | cut -f 2

#3.
find /etc -printf '%T+\t%p\n' 2>/dev/null | sort -k1 -r | head -n1 | cut -f2
find /etc -maxdepth 1 -printf '%T+\t%p\n' 2>/dev/null | sort -k1 -r | head -n1 | cut -f2
