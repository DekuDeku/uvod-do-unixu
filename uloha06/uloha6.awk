#!/usr/bin/awk -f

BEGIN {
	RS="<"
}

function printHtml(html){
	gsub(/&lt/,"<",html)
	gsub(/&gt/,">",html)
	gsub(/&quot/,"\"",html)
	gsub(/&amp/,"\\&",html)
	print html
}

/^a/ {
	for (i=1;i<=NF;i++) {
		if (match($i,/^href="[^>]*"/)) {
			link = substr($i, 7, RLENGTH - 7)
			printHtml(link)
		}
		else if (match($i,/^href=[^>]*/)) {
			link = substr($i, 6, RLENGTH - 5)
			printHtml(link)
		}

		if (match($i,/>/)) {
			break
		}
	}
}

