#1
grep -i -E rAVeN raven.txt
#2
grep -E ^$ raven.txt
#3
grep -E \(rep\|word\|more\) raven.txt
#4
grep -E 'p.*p.*ore' raven.txt
#5
grep -E '^[^A-Z]' raven.txt
#6
grep -E '\-$' raven.txt | wc -l
#7
grep -o -P '[a-zA-Z-]*ore(?=[^a-zA-Z-])' raven.txt
#8
grep -o -P '(?<=[^a-zA-Z-])(f|F)[a-zA-Z-]*' raven.txt
#9
grep -o -P '(?<=[^a-zA-Z-])([a-zA-Z])\S*\s+\1\S*' raven.txt
