#!/bin/sh

#linearCoord x y => 1d linearized coordinate from 2d array of size [tilesX,tilesY]
linearCoord() {
	echo "$(($tilesX * $2 + $1))"
}

#setTile x y tile
setTile() {
	local n=$(linearCoord $1 $2)
	tiles[n]=$3
}

#getTile x y => tiles[x,y]
getTile() {
	local n=$(linearCoord $1 $2)

	local val=${tiles[n]}
	case $val in
		"")
			echo "." ;;
		*)
			echo $val ;;
	esac

	#echo ${tiles[n]}
}

#loadLevel <1 levelLines
loadLevel() {
	local first
	local line
	local i

	tilesX=0
	tilesY=1
	
	#read first line, that isn't comment (comments lines start with # on FIRST character)
	local cmnt="#*"
	while read line
	do
		case "$line" in
			$cmnt)
				echo "comment: ""$line" >> $logfile;;
			*)
				echo "first line: ""$line" >> $logfile
				break ;;
		esac
	done

	for i in $line
	do
		tilesX=$(($tilesX + 1))
	done

	loadLine 1 $line
	while read first line
	do
		tilesY=$(($tilesY + 1))
		case "$first" in
			REPLAY)
				moves=$line
				break ;;
			$cmnt)
				echo "comment: ""$line" >> $logfile
				tilesY=$((tilesY - 1))
				continue ;;
			*)
				loadLine $tilesY $first $line ;; #intentionally gets separated into separate args of loadLine
		esac
	done

	backupLevel
}

#loadLine y tile1 tile2 tile3 ... tileN
loadLine() {
	local y=$1
	local x=1
	shift
	while [ $# -gt 0 ]
	do
		setTile $x $y $1
		if isPlayer $1
		then
			playerX=$x
			playerY=$y
		fi
		x=$(($x + 1))
		shift
	done
}

#backupLevel : copies tiles array to backupTiles, so it can be saved in replays, or restarted
backupLevel() {
	local i
	local j

	for i in $(seq $tilesX)
	do
		for j in $(seq $tilesY)
		do
			local n=$(linearCoord $i $j)
			backupTiles[$n]=${tiles[$n]}
		done
	done

	originalPX=$playerX
	originalPY=$playerY
}

#restoreLevel : copies backupTiles array to tiles, so you can start playing the level again
restoreLevel() {
	local i
	local j

	for i in $(seq $tilesX)
	do
		for j in $(seq $tilesY)
		do
			local n=$(linearCoord $i $j)
			tiles[$n]=${backupTiles[$n]}
		done
	done

	recording=""
	playerX=$originalPX
	playerY=$originalPY
}

echoBackup() {
	local i
	local j

	for j in $(seq $tilesX)
	do
		for i in $(seq $tilesY)
		do
			local n=$(linearCoord $i $j)
			local t=${backupTiles[$n]}
			printf "$t "
		done
		printf "\n"
	done
}

#tileColor tile => color
tileColor() {
	case $1 in
		w)
			echo blue ;;
		p)
			echo green ;;
		P)
			echo green ;;
		x)
			echo red ;;
		X)
			echo magenta ;;
		T)
			echo white ;;
		.)
			echo black ;;
		*)
			echo black ;;
	esac
}

#renderTiles
renderTiles() {
	local i
	local j

	clear
	for i in $(seq $tilesX)
	do
		for j in $(seq $tilesY)
		do
			local t=$(getTile $i $j)
			local c=$(tileColor $t)
			fillTile $i $j $c
		done
	done
}

#movePlayer {left|right|up|down}
movePlayer() {
	local dx=0
	local dy=0
	case "$1" in
		left)
			dx="-1" ;;
		right)
			dx="1" ;;
		up)
			dy="-1" ;;
		down)
			dy="1" ;;
		*)
			echo "unknown player move: "$1 >> $logfile
			return 1 ;;
	esac
	local mov=$1

	local x1=$(($playerX + $dx))
	local y1=$(($playerY + $dy))
	local x2=$(($x1 + $dx))
	local y2=$(($y1 + $dy))

	local p="$(getTile $playerX $playerY)"
	local t1="$(getTile $x1 $y1)"
	local t2="$(getTile $x2 $y2)"
	
	set $(moveSequence "$p" "$t1" "$t2") 
	echo "moving sequence:" $p $t1 $t2 "->" $1 $2 $3 \( $4 \) >> $logfile

	#only change tiles if there was actual movement
	if [ "$4" -ge "1" ]
	then
		recording=$recording" "$mov

		echo "moving player..." >> $logfile
		setTile $playerX $playerY $1
		setTile $x1 $y1 $2
		
		fillTile $playerX $playerY $(tileColor $1)
		fillTile $x1 $y1 $(tileColor $2)

		#only set 3rd tile if box was moved
		if [ "$4" -ge "2" ]
		then
			echo "moving box..." >> $logfile
			setTile $x2 $y2 $3
			fillTile $x2 $y2 $(tileColor $3)
		fi

		#change player location, if player moved
		if isPlayer $2
		then
			playerX=$x1
			playerY=$y1
		fi
	fi
}

#moveSequence p t1 t2 => a b c moved
# p is player ({p|P})
# t1 is tile behind player in some direction
# t2 is 2 tiles behind player in same direction as t1
# a b c = sequence of tiles after moving
# moved =
#	0 -> no move
#	1 -> player moved
#	2 -> player pushed box
moveSequence() {
	if isFree $2
	then
		#move player to empty tile
		echo "$(removePlayer $1)" "$(setPlayer $2)" "$3" "1"
		return 1
	elif isBox $2 && isFree $3
	then
		#player pushes box
		echo "$(removePlayer $1)" "$(setPlayer $2)" "$(setBox $3)" "2"
		return 2
	else
		#no player movement
		echo "$1" "$2" "$3" "0"
		return 0
	fi
}

#isFree tile => return: it's possible to move player/tile to this
isFree() {
	case "$1" in
		[.T])
			return 0 ;;
		*)
			return 1 ;;
	esac
}

#isBox tile => return: tile is box
isBox() {
	case "$1" in
		[xX])
			return 0 ;;
		*)
			return 1 ;;
	esac
}

#isPlayer tile => return: tile is player
isPlayer() {
	case "$1" in
		[pP])
			return 0 ;;
		*)
			return 1 ;;
	esac
}

#removePlayer tile => tileWithoutPlayer
removePlayer() {
	case "$1" in
		p)
			echo . ;;
		P)
			echo T ;;
		*)
			echo "removePlayer called with incorrect parameters: $1" >> $logfile
			#this is error, but at least let game in somewhat consistent state
			echo p ;;
	esac
}

#setPlayer tile => tileWithPlayer
setPlayer() {
	case "$1" in
		[.x])
			echo p ;;
		[TX])
			echo P ;;
		#player can also move to place where box was
		*)
			echo "setPlayer called with incorrect parameters: $1" >> $logfile
			#this is error, but at least let game in somewhat consistent state
			echo p ;;
	esac
}

#setBox tile => tileWithBox
setBox() {
	#box will only move on . or T
	case "$1" in
		.)
			echo x ;;
		T)
			echo X ;;
		*)
			echo "setBox called with incorrect parameters: $1" >> $logfile
			#this is error, but at least let game in somewhat consistent state
			echo x ;;
	esac
}

#fillTile x y color={black|red|green|yellow|blue|magenta|cyan|white} [small]
fillTile() {
	local x1=$((($1 - 1) * $tileSize * 2)) # *2, because char is 1x2 size
	local y1=$((($2 - 1) * $tileSize))
	local x2=$(($x1 + $tileSize * 2 -1 )) # -1, because it's supposed to be excluding edge, not including
	local y2=$(($y1 + $tileSize - 1))

	if [ "$4" == "small" ]
	then
		y1=$(($y1 + 1))
		y2=$(($y2 - 1))
		x1=$(($x1 + 2))
		x2=$(($x2 - 2))
	fi

	case "$3" in
		black)
			c=40 ;;
		red)
			c=41 ;;
		green)
			c=42 ;;
		yellow)
			c=43 ;;
		blue)
			c=44 ;;
		magenta)
			c=45 ;;
		cyan)
			c=46 ;;
		white)
			c=47 ;;
		*)
			c=47 ;;
	esac
	local line=""
	local i=""
	for i in $(seq $x1 $x2)
	do
		#line="${line}#"
		line="${line} "
	done

	local ech=""

	for i in $(seq $y1 $y2)
	do
		ech=${ech}"\e[${i};${x1}f\e[${c}m${line}" #\e[Y;Xf sets position, \e[COLORm sets color (fg/bg depends on number)
	done
	echo -e "${ech}""\e[H\e[0m" #\e[H sets cursor home (to avoid weird graphics glitches) \e[0m resets cursor color (so that exiting doesn't break anything)
}

saveReplay() {
	local loc
	local ans

	echo "save replay as: "
	read loc
	if [ -e $loc ]
	then
		echo "replace existing file (yes/y to save)?"
		read ans
		case "$ans" in
			yes);;
			y);;
			*)
				echo "file not saved!"
				read
				return 1
		esac
	fi

	echo "saving to: "$loc >> $logfile
	echoBackup >> $logfile
	echo "REPLAY" $recording >> $logfile

	echoBackup > $loc
	echo "REPLAY" $recording >> $loc

	echo "saved to "$loc"!"
	read

	return 0
}

#isWin => if game is won
isWin() {
	local i
	local j

	for i in $(seq $tilesX)
	do
		for j in $(seq $tilesY)
		do
			local t=$(getTile $i $j)
			if [ "$t" == "T" -o "$t" == "P" ]
			then
				return 1
			fi
		done
	done
	return 0
}

#play : plays the currently loaded level
play() {
	local input

	while :	
	tput home
	do	
		tput home
		read -rsn1 input
		case $input in
			w)
				movePlayer up ;;
			s)
				movePlayer down ;;
			a)
				movePlayer left ;;
			d)
				movePlayer right ;;
			p)
				tput cnorm
				saveReplay
				tput civis
				renderTiles ;;
			u)
				clear
				return 0 ;;
			r)
				restoreLevel
				renderTiles ;;
		esac

		if isWin
		then
			echo "you won!"
			echo "do you want to save replay (y/yes to save)?"
			local ans
			read ans
			case "$ans" in
				yes);;
				y);;
				*)
					return 1
			esac
			tput cnorm
			saveReplay
			clear
			return 0
		fi
	done
}

#replay : replays currently loaded level
replay() {
	local move

	for move in $moves
	do
		movePlayer $move
		sleep 0.07
	done

	echo "press enter to exit.."
	read
	clear
}

# main : ...
main() {
	cols=$(tput cols)
	lines=$(tput lines)
	
	tileSize=4
	
	logfile="log.txt"
	touch "$logfile"
	echo "" > $logfile
	
	recording=""
	if [ "$1" == "" ]
	then
		echo "level or replay file must be specified in the arguments!"
		return 1
	elif [ ! -e "$1" ]
	then
		echo "file $1 does not exist!"
		return 1
	fi

	#hide cursor
	tput civis

	moves="no"
	loadLevel < $1

	tileSize=$(($cols / $tilesX))
	tileSize=$(($tileSize / 2))
	altSize=$(($lines / $tilesY))
	if [ $altSize -lt $tileSize ]
	then
		tileSize=$altSize
	fi
	
	echo "tile size was determined to be: "$tileSize >> $logfile

	##terminal size check for static tile size
	#if [ $cols -lt $(($tilesX * $tileSize)) -o $lines -lt $(($tilesY * $tileSize)) ]
	#then
		#echo "level is too big for current terminal size"
		#return 1
	#fi

	renderTiles
	case "$moves" in
		"no") #if file did not have REPLAY field, then it's just a level
			echo "entering play mode on $1" >> $logfile
			play ;;
		*)
			echo "entering replay mode on $1" >> $logfile
			replay ;;
	esac

	#reset visibility
	tput cnorm
}

main "$@"
#DOCS:
# maybe man file?
# levels have to be rectangles, explain symbols, explain comments (and how they can be used to autolaunch levels or replays)
# some code explanation (not in man?, but most of it is on comments)
# gameplay, shortcuts, parameters
