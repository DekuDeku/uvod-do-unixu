#!/bin/sh
#z /etc/group uříznu seznam uživatelů,
#pomocí awk je zpočítám,
#pastnu je na začátek /etc/group,
#a setřídim podle počtu uživatelů
cat /etc/group | cut -d: -f4 | awk 'BEGIN {FS=","} {print NF}' | paste - /etc/group -d: | sort -n -r -k1,2 > tmp.$$
#poté grepnu ty, které mají max. počet uživatelů a odříznu začátek = počet uživatelů
cat tmp.$$ | grep -e "^$(head -n1 tmp.$$ | cut -f1 -d:)" | cut -f2- -d:
rm tmp.$$
