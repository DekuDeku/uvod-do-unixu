#!/bin/sh
# Vytvořím sekvence tak, abych je mohl paste-nout za setřízené soubory
seq 1 $(wc -l < social.txt) > t1.$$
seq 1 $(wc -l < beverly_hills.txt) > t2.$$
seq 1 $(wc -l < actor.txt) > t3.$$
# Setřídím soubory
sort -f social.txt > s1.$$
sort -f beverly_hills.txt > s2.$$
sort -f actor.txt > s3.$$
# Pastnu za ně čísla řádků, aby se mi to dobře joinovalo a oddělím je :
paste s1.$$ t1.$$ -d : > p1.$$
paste s2.$$ t2.$$ -d : > p2.$$
paste s3.$$ t3.$$ -d : > p3.$$
# Joinnu všechny 3 soubory a cutnu z nich 1. sloupec
join -t : p1.$$ p2.$$ | join -t : - p3.$$ | cut -d: -f1
# Removnu dočasné soubory
rm {t,s,p}{1,2,3}.$$
