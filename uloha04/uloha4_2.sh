#!/bin/sh
# Setřídím soubory, protože tam je země s divným A, kvůli které nejde join-ovat
sort countrycodes_en.csv -t\; -k1 > 1.$$
sort kodyzemi_cz.csv -t\; -k4 > 2.$$
# Join-nu správné sloupce
join -t \; 1.$$ 2.$$ -1 1 -2 4 | cut -d\; -f1
# Smažu dočasné soubory
rm {1,2}.$$
