pocet=0;
cat /etc/passwd | while read x
do
   pocet=`expr $pocet + 1`
done
echo $pocet
# Tohle nefunguje, protože dva příkazy v pipě jsou 2 procesy a ten
# while proces edituje $pocet v prostředí toho procesu, zatímco
# echo zobrazí $pocet z prostředí toho otcovského procesu,
# takže tam je 0

cat /etc/passwd | {
   pocet=0
   while read x
   do
      pocet=`expr $pocet + 1`
   done
   echo $pocet
}
# Tohle funguje, protože narozdíl od předchozího procesu je $pocet
# z nového procesu vypsán pořád v tom stejném procesu, kde se to počítá

pocet=0
while read x
do
   pocet=`expr $pocet + 1`
done </etc/passwd
echo $pocet
# Tohle funguje, protože tady se nevytváří nový proces pipou

pocet=0
while read x </etc/passwd
do
   pocet=`expr $pocet + 1`
done
echo $pocet
# Tohle nefunguje, protože je tam špatně použitý read ve while
# tím způsobem, že to pokaždé otevře nový stream a přečte první řádek
# a proto to vrací pořád true a nikdy to nezkončí
